package it.unibo.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.StringTokenizer;

import org.apache.commons.io.FileUtils;

public class PhotoManager {

	List<List<String>> lines = new ArrayList<>();
	Scanner inputStream;
	List<String> percorsi = new ArrayList<String>();
	Map<String, ArrayList<Double>> mappa = new HashMap<String, ArrayList<Double>>();
	Map<String, String> finale = new HashMap<String, String>();

	public PhotoManager() {
		savePhotos();
		checkPhotos();
		createFolder();
	}

	private void savePhotos() {
		String fileName= ApplicationController.path + "/SendNudes.csv";
		File file= new File(fileName);
		Reader r = null;
		try {
			r = new FileReader(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		BufferedReader bf = new BufferedReader(r);
		String values=null;
		String line=null;
		int j=0;

		try {
			while((values=bf.readLine())!=null) {
				if(values.isEmpty()==false) {
					List<String> arr = new ArrayList<String>();
					StringTokenizer str = new StringTokenizer(values, ",\n");
					while(str.hasMoreTokens()==true && j!=0){
						line= str.nextToken(",\n");
						arr.add(line);
					}
					if(j!=0) {

						lines.add(arr);

					}
					j++;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		for(int i = 0; i < lines.size(); i++) {
			lines.get(i);
			ArrayList<Double> ar = new ArrayList<Double>();
			for(int l = 1; l <= 5; l++) {
				ar.add(Double.parseDouble(lines.get(i).get(l)));
				mappa.put(lines.get(i).get(0), ar);
			}
		}
		for(int i = 0; i < lines.size(); i++) {
			lines.get(i);
			percorsi.add(lines.get(i).get(0));
		}
		try {
			bf.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * Check, for every photo, the csv value and associate the photo to a string that describe its type.
	 */
	private void checkPhotos() {
		for(String string : percorsi) {
			ArrayList<Double> ar = mappa.get(string);
			if(ar.get(3) >= ar.get(4)) {
				if(ar.get(0) > ar.get(1)) {
					if(ar.get(0) > ar.get(2)) {
						finale.put(string, "Sopra");
					} else {
						finale.put(string, "Sotto");
					}
				} else {
					if(ar.get(1) > ar.get(2)) {
						finale.put(string, "Buona");
					} else {
						finale.put(string, "Sotto");
					}
				}
			} else {
				finale.put(string, "Mossa");
			}
		} 
	}
	/**
	 *  Create a directory for every type of photo, if it doesn't exists already.
	 */
	private void createFolder() {
		File sotto = new File(ApplicationController.path + "/Sottoesposte");
		if(!sotto.exists()) {
			new File(ApplicationController.path + "/Sottoesposte").mkdirs();
		}
		File sopra = new File(ApplicationController.path + "/Sovraesposte");
		if(!sopra.exists()) {
			new File(ApplicationController.path + "/Sovraesposte").mkdirs();
		}
		File buone = new File(ApplicationController.path + "/Buone");
		if(!buone.exists()) {
			new File(ApplicationController.path + "/Buone").mkdirs();
		}
		File mosse = new File(ApplicationController.path + "/Mosse");
		if(!mosse.exists()) {
			new File(ApplicationController.path + "/Mosse").mkdirs();
		}
		for(String string : percorsi) {
			if(finale.get(string).equals("Mossa")) {
				try {
					FileUtils.copyFileToDirectory(new File(ApplicationController.path + "/" + string), new File(ApplicationController.path + "/Mosse/"));
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				if(finale.get(string).equals("Buona")) {
					try {
						FileUtils.copyFileToDirectory(new File(ApplicationController.path + "/" + string), new File(ApplicationController.path + "/Buone/"));
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				if(finale.get(string).equals("Sotto")) {
					try {
						FileUtils.copyFileToDirectory(new File(ApplicationController.path + "/" + string), new File(ApplicationController.path + "/Sottoesposte/"));
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				if(finale.get(string).equals("Sopra")) {
					try {
						FileUtils.copyFileToDirectory(new File(ApplicationController.path + "/" + string), new File(ApplicationController.path + "/Sovraesposte/"));
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
		try {
			FileUtils.forceDelete(new File(ApplicationController.path + "/SendNudes.csv"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

