package it.unibo.view;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;

public interface GUIFactory {
	
	public JFrame createFrame(String title);
	
	public JButton createButton(String text);
	
	public JDialog createDialog();
}
