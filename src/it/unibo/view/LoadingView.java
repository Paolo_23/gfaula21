package it.unibo.view;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import it.unibo.controller.ApplicationController;

public class LoadingView extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ApplicationController ac;
	private JFrame frame;
	public JPanel panel;

	public LoadingView(ApplicationController appcontr) throws MalformedURLException {
		this.ac = appcontr;
		this.frame = ApplicationController.gui.createFrame("ED.IT - Loading");
		frame.setSize(1000, 750);
		loadPanel();
		loadgif();
		addAl();
		frame.setVisible(true);
		done();
	}

	private void checkDone() {
		while(!check()) {
		}
		this.frame.dispose();
		ac.endLoading();
	}

	private boolean check() {
		File tmpDir = new File(ApplicationController.path + "/SendNudes.csv");
		boolean exists = tmpDir.exists();
		return exists;
	}

	private void done() {
		Thread thread = new Thread(){
			public void run(){
				checkDone();
			}
		};
		thread.start();
	}

	private void loadPanel() {
		this.panel = new JPanel();
		panel.setSize(1000, 750);
		this.frame.add(panel);
	}

	private void loadgif() throws MalformedURLException {		
		URL url = this.getClass().getResource("/animazione.gif");
		Icon icon = new ImageIcon(url);
		JLabel label = new JLabel(icon);
		label.setSize(1000, 750);
		panel.add(label);
	}
	
	private void addAl() {
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				try {
					Runtime.getRuntime().exec("taskkill /IM \"SendNudes.exe\" /F");
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				System.exit(0);
			}
		});
	}
}
