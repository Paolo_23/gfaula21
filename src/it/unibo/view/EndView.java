package it.unibo.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import it.unibo.controller.ApplicationController;
import it.unibo.utils.BackgroundPanel;
import it.unibo.utils.ImageLoaderUtils;

public class EndView extends JFrame implements ActionListener {
	
	private static final long serialVersionUID = 1L;
	private ApplicationController ac;
	private String imagepath = "/loading.jpg";
	private String logopath = "/logo.png";
	private JFrame frame;
	private JPanel panel;
	private ImageLoaderUtils il = new ImageLoaderUtils();
	private JButton b1,b2,b3,b4,b5;
	
	public EndView(ApplicationController appcontr) {
		this.ac = appcontr;
		createFrame();
	}
	
	private void createFrame() {
		frame = ApplicationController.gui.createFrame("ED.IT");
		frame.setLayout(new GridLayout(1, 2));
		frame.setIconImage(il.loadImage(logopath));
		createPanels();
		frame.add(panel);
		frame.setVisible(true);
	}
	
	private void createPanels() {
		panel = new BackgroundPanel(il.loadImage(imagepath));
		panel.setLayout(new BorderLayout());
		createComponents();
	}
	
	private void createComponents() {
		b1 = ApplicationController.gui.createButton("Return to Main Menu");
		b2 = ApplicationController.gui.createButton("Sovraesposte");
		b3 = ApplicationController.gui.createButton("Sottoesposte");
		b4 = ApplicationController.gui.createButton("Mosse");
		b5= ApplicationController.gui.createButton("Buone");
		JPanel westp = new JPanel(new GridLayout(5,1));
		b1.addActionListener(this);
		b2.addActionListener(this);
		b3.addActionListener(this);
		b4.addActionListener(this);
		b5.addActionListener(this);;
		westp.add(b1);
		westp.add(b2);
		westp.add(b5);
		westp.add(b3);
		westp.add(b4);
		panel.add(westp, BorderLayout.WEST);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == b1) {
			this.frame.setVisible(false);
			ac.returnToMain();
		}
		if(e.getSource() == b2) {
			this.openDirectory(ApplicationController.path + "/" + b2.getText());	
		}
		if(e.getSource() == b3) {
			this.openDirectory(ApplicationController.path + "/" + b3.getText());	
		}
		if(e.getSource() == b4) {
			this.openDirectory(ApplicationController.path + "/" + b4.getText());	
		}
		if(e.getSource() == b5) {
			this.openDirectory(ApplicationController.path + "/" + b5.getText());	
		}
	}
	
	private void openDirectory(String path) {
		File dirToOpen;
		Desktop.getDesktop();
		try {
			dirToOpen = new File(path);
			try {
				if (Desktop.isDesktopSupported()) {
					Desktop.getDesktop().open(dirToOpen);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (IllegalArgumentException iae) {
			System.out.print("cartella non esiste");
		}
	}
}
